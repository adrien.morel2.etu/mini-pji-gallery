import sqlite3
import sys
from sqlite3 import Error

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import QObject, pyqtSignal


class Diaporama(QtWidgets.QMainWindow):
    def __init__(self, img_id_list: list):
        super().__init__()
        self.setFocus()

        self.img_id_list = img_id_list
        self.current = 0

        self.app = QtWidgets.QApplication.instance()

        self.screen = self.app.primaryScreen()
        self.screen_width = self.screen.size().width()
        self.screen_height = self.screen.size().height()

        self.pixmap = QPixmap(self.screen_width, self.screen_height)
        self.pixmap.fill(QColor('black'))

        self.img_widget = QtWidgets.QLabel()

        self.setCentralWidget(self.img_widget)
        # self.img_widget.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus | Qt.WindowStaysOnTopHint)
        # self.img_widget.setWindowFlags(Qt.FramelessWindowHint)

        # Cacher le curseur
        # self.img_widget.setCursor(Qt.BlankCursor)

        self.img_widget.setStyleSheet("background-color: black;")

        self.img_widget.setGeometry(0, 0, self.screen_width, self.screen_height)
        self.img_widget.setWindowTitle('Diaporama')
        self.img_widget.setAlignment(Qt.AlignCenter | Qt.AlignTop)
        self.img_widget.setPixmap(self.pixmap)

        self.setFocus()
        self.update_img()

    def update_img(self):
        try:
            conn = sqlite3.connect("GalleryDatabase.db")
            c = conn.cursor()
            c.execute("""SELECT imgpath FROM gallery where numero = ?""", (self.img_id_list[self.current],))
            row = c.fetchone()
            conn.close()
            self.img_widget.clear()
            self.pixmap.fill(QColor('green'))
            pixmap = QPixmap(row[0]).scaled(self.screen_width, self.screen_height, Qt.KeepAspectRatio)
            self.pixmap = pixmap
            self.img_widget.setPixmap(self.pixmap)
        except Error as e:
            print(e)

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        if event.key() == Qt.Key_Left:
            self.on_prev()
        elif event.key() == Qt.Key_Right:
            self.on_next()
        elif event.key() == Qt.Key_Escape:
            self.close()

    def on_next(self):
        self.current = (self.current + 1) % len(self.img_id_list)
        self.update_img()

    def on_prev(self):
        self.current = (self.current - 1) % len(self.img_id_list)
        self.update_img()
