import re
import sqlite3

from PyQt5 import QtWidgets, QtGui, QtCore


class QCustomQWidget(QtWidgets.QWidget):
    def __init__(self, idid, img, titre, annee, commentaire, parent=None):
        super(QCustomQWidget, self).__init__(parent)
        self.idid = idid
        self.img = img
        self.textQVBoxLayout = QtWidgets.QVBoxLayout()

        self.titreQLabel = QtWidgets.QLabel(str(idid) + " : " + titre)
        self.commentaireQLabel = QtWidgets.QLabel(commentaire)
        self.anneeQLabel = QtWidgets.QLabel(str(annee))
        self.textQVBoxLayout.addWidget(self.titreQLabel)
        self.textQVBoxLayout.addWidget(self.anneeQLabel)
        self.textQVBoxLayout.addWidget(self.commentaireQLabel)

        self.allQHBoxLayout = QtWidgets.QHBoxLayout()

        self.iconQLabel = QtWidgets.QLabel()
        tmp = QtGui.QPixmap(img)
        pixmap = tmp.scaled(120, 120, QtCore.Qt.KeepAspectRatio)
        self.iconQLabel.setPixmap(pixmap)
        self.allQHBoxLayout.addWidget(self.iconQLabel, 0)
        self.allQHBoxLayout.addLayout(self.textQVBoxLayout, 1)
        self.setLayout(self.allQHBoxLayout)


def str_to_params(string: str):
    return re.sub(' +', ' ', string.strip()).replace(' ', ' AND ')


class Request:
    def __init__(self, search_text: str = "", order: str = ""):
        pattern = re.compile("[0-9]{4}-[0-9]{4}")
        t = str_to_params(search_text)
        self.search = " WHERE titre like '%" + t + "%' OR commentaire like '%" + t + "%' OR annee like '%"+t+"%' OR numero like '%"+t+"%'"
        if search_text.strip() == "":
            self.search = ""
        elif pattern.match(search_text):
            tmp = search_text[0:9]
            tmp = tmp.split('-')
            self.search = """ WHERE annee BETWEEN """ + tmp[0] + """ AND """ + tmp[1]
        self.order = """ ORDER BY """ + order
        if order == "":
            self.order = ""

    def process_to_widget(self):
        query = """SELECT numero,imgpath,titre,annee,commentaire FROM gallery""" + self.search + self.order
        conn = sqlite3.connect("GalleryDatabase.db")
        c = conn.cursor()
        c.execute(query)
        rows = c.fetchall()
        conn.close()
        for i in rows:
            yield QCustomQWidget(i[0], i[1], i[2], i[3], i[4])
