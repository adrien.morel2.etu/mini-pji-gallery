import os

import shutil

from PyQt5 import QtWidgets, QtCore, QtGui, Qt
import sqlite3
from sqlite3 import Error

from PyQt5.QtCore import pyqtSignal

from PyQt5.QtGui import QIntValidator

from RequestHandler import QCustomQWidget


class PopUpModifier(QtWidgets.QMainWindow):
    window_closed = pyqtSignal()

    def __init__(self, img_id: QCustomQWidget, parent=None):
        super().__init__(parent)

        self.id = img_id.idid

        self.saisie_numero = QtWidgets.QLineEdit()
        self.saisie_titre = QtWidgets.QLineEdit()
        self.saisie_annee = QtWidgets.QLineEdit()
        self.saisie_annee.setValidator(QIntValidator())
        self.saisie_annee.setMaxLength(4)
        self.saisie_commentaire = QtWidgets.QTextEdit()
        self.saisie_commentaire.setMaximumHeight(50)

        self.supprimer_btn = QtWidgets.QPushButton("Supprimer")
        self.modifier_btn = QtWidgets.QPushButton("Modifier")

        self.photo = QtWidgets.QLabel()

        central_widget = QtWidgets.QWidget()
        self.setCentralWidget(central_widget)

        grid_layout = QtWidgets.QGridLayout(central_widget)

        grid_layout.addWidget(self.photo, 0, 0, 1, 3)
        grid_layout.addWidget(QtWidgets.QLabel("Numéro"))
        grid_layout.addWidget(self.saisie_numero, 1, 1, 1, 2)
        grid_layout.addWidget(QtWidgets.QLabel("Titre : "))
        grid_layout.addWidget(self.saisie_titre, 2, 1, 1, 2)
        grid_layout.addWidget(QtWidgets.QLabel("Année : "), 3, 0, 1, 1)
        grid_layout.addWidget(self.saisie_annee, 3, 1, 1, 2)
        grid_layout.addWidget(QtWidgets.QLabel("Commentaire : "), 4, 0, 1, 1)
        grid_layout.addWidget(self.saisie_commentaire, 4, 1, 2, 2)
        grid_layout.addWidget(self.supprimer_btn, 6, 0, 1, 1)
        grid_layout.addWidget(self.modifier_btn, 6, 1, 1, 2)

        # self.resize(640, 480)

        self.modifier_btn.clicked.connect(self.on_modifier)
        self.supprimer_btn.clicked.connect(self.on_supprimer)
        self.on_load()

    def on_load(self):
        try:
            conn = sqlite3.connect("GalleryDatabase.db")
            c = conn.cursor()
            c.execute("""SELECT * FROM gallery where numero = ?""", (self.id,))
            row = c.fetchone()
            conn.close()

            self.photo.setText("")
            pixmapOriginal = QtGui.QPixmap(row[1])
            pixmap = pixmapOriginal.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
            self.photo.setPixmap(pixmap)
            self.photo.setScaledContents(False)

            self.saisie_numero.setText(row[0])
            self.saisie_titre.setText(row[2])
            self.saisie_annee.setText(str(row[3]))
            self.saisie_commentaire.setText(row[4])
        except Error as e:
            print(e)

    def on_modifier(self):
        query = '''update gallery set numero = ?, titre = ?,annee = ?,commentaire = ? where numero = ?;'''
        params = (self.saisie_numero.text(), self.saisie_titre.text(), int(self.saisie_annee.text()), self.saisie_commentaire.toPlainText(), self.id)
        try:
            conn = sqlite3.connect("GalleryDatabase.db")
            c = conn.cursor()
            c.execute(query, params)
            conn.commit()
            conn.close()
            self.close()
        except Error as e:
            print(e)

    def on_supprimer(self):
        query = '''delete from gallery where numero = ?'''
        params = (self.id,)
        try:
            conn = sqlite3.connect("GalleryDatabase.db")
            c = conn.cursor()
            c.execute(query, params)
            conn.commit()
            conn.close()
            self.close()
        except Error as e:
            print(e)

    def closeEvent(self, event):
        self.window_closed.emit()
        event.accept()
