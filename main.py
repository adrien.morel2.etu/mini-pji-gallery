import os
import sys
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QListView, QAbstractItemView, QListWidgetItem

import sqlite3
from sqlite3 import Error

from A_traiter_popup import PopUpTraiter
from Diaporama import Diaporama
from Modify_popup import PopUpModifier
from RequestHandler import Request

ICON_SIZE = 100
A_TRAITER_DIR = ""
CONSULTER_DIR = ""

ORDER_BY_ARRAY = ['numero + 0 ASC', 'numero + 0 DESC', 'annee ASC', 'annee DESC', 'titre ASC', 'titre DESC',
                  'rowid DESC']


class StyledItemDelegate(QtWidgets.QStyledItemDelegate):
    def initStyleOption(self, option, index):
        super().initStyleOption(option, index)
        option.text = option.fontMetrics.elidedText(
            index.data(), QtCore.Qt.ElideRight, ICON_SIZE
        )


def load_images(directory):
    it = QtCore.QDirIterator(
        directory,
        ["*.jpg", "*.png", "*.jpeg"],
        QtCore.QDir.Files,
        QtCore.QDirIterator.Subdirectories,
    )
    while it.hasNext():
        filename = it.next()
        print(filename)
        yield filename


def create_database():
    sql_create_gallery_table = """ CREATE TABLE IF NOT EXISTS gallery (
                                            numero text,
                                            imgpath text,
                                            titre text,
                                            annee int,
                                            commentaire text
                                        ); """
    try:
        conn = sqlite3.connect("GalleryDatabase.db")
        c = conn.cursor()
        c.execute(sql_create_gallery_table)
        conn.close()
    except Error as e:
        print(e)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.traiter_btn = QtWidgets.QPushButton("A traiter")
        self.consult_btn = QtWidgets.QPushButton("Consulter")
        self.currentpage_is_traiter = None

        self.search_text = QtWidgets.QLabel("Rechercher : ")
        self.search_bar = QtWidgets.QLineEdit()

        self.order_text = QtWidgets.QLabel("Trier par : ")
        self.order_selection = QtWidgets.QComboBox()
        self.order_selection.addItems(
            ['numéro ↑', 'numéro ↓', 'date ↑', 'date ↓', 'titre ↑', 'titre ↓', 'dernier ajout'])
        self.current_filter = ORDER_BY_ARRAY[0]
        self.diapo_btn = QtWidgets.QPushButton("Diaporama")
        self.modifier_btn = QtWidgets.QPushButton("Modifier")

        self.pixmap_lw = QtWidgets.QListWidget()

        self.pixmap_lw.itemDoubleClicked.connect(self.double_clicked)

        self.pixmap_lw.setIconSize(100 * QtCore.QSize(1, 1))
        #self.pixmap_lw.setMovement(QListView.Static)
        self.pixmap_lw.setVerticalScrollMode(QtWidgets.QListWidget.ScrollPerPixel)
        self.pixmap_lw.setResizeMode(QListView.Adjust)

        delegate = StyledItemDelegate(self.pixmap_lw)
        self.pixmap_lw.setItemDelegate(delegate)

        central_widget = QtWidgets.QWidget()
        self.setCentralWidget(central_widget)
        self.grid_layout = QtWidgets.QGridLayout(central_widget)

        self.grid_layout.addWidget(self.traiter_btn, 0, 0, 2, 2)
        self.grid_layout.addWidget(self.consult_btn, 0, 2, 2, 2)
        self.grid_layout.addWidget(self.search_text, 2, 0, 1, 1)
        self.grid_layout.addWidget(self.search_bar, 2, 1, 1, 3)
        self.grid_layout.addWidget(self.order_text, 3, 0, 1, 1)
        self.grid_layout.addWidget(self.order_selection, 3, 1, 1, 1)
        self.grid_layout.addWidget(self.diapo_btn)
        self.grid_layout.addWidget(self.modifier_btn)
        self.grid_layout.addWidget(self.pixmap_lw, 4, 0, 1, 4)

        self.resize(640, 480)

        self.timer_loading = QtCore.QTimer()
        self.timer_loading.setInterval(50)
        self.timer_loading.timeout.connect(self.load_image)
        self.filenames_iterator = None

        self.start_loading_a_traiter()
        self.traiter_btn.clicked.connect(self.start_loading_a_traiter)
        self.consult_btn.clicked.connect(self.start_loading_consulter)
        self.search_bar.textChanged.connect(self.search_request_handler)
        self.order_selection.currentIndexChanged.connect(self.order_by_handler)
        self.diapo_btn.clicked.connect(self.on_diapo)
        self.modifier_btn.clicked.connect(self.on_modify)

    def start_loading_a_traiter(self):
        self.currentpage_is_traiter = True
        self.pixmap_lw.setSelectionMode(QAbstractItemView.MultiSelection)
        self.remove_consulter_toolbar()
        self.pixmap_lw.setViewMode(QListView.IconMode)
        if self.timer_loading.isActive():
            self.timer_loading.stop()
        self.filenames_iterator = load_images(A_TRAITER_DIR)
        self.pixmap_lw.clear()
        self.timer_loading.start()

    def start_loading_consulter(self):
        self.timer_loading.stop()
        self.currentpage_is_traiter = False
        self.pixmap_lw.setSelectionMode(QAbstractItemView.SingleSelection)
        self.add_consulter_toolbar()
        self.pixmap_lw.clear()
        self.pixmap_lw.setViewMode(QListView.ListMode)
        request = Request(order=self.current_filter)
        self.pixmap_lw.clear()
        for wi in request.process_to_widget():
            it = QListWidgetItem(self.pixmap_lw)
            it.fp = wi.idid
            it.setSizeHint(wi.sizeHint())
            self.pixmap_lw.addItem(it)
            self.pixmap_lw.setItemWidget(it, wi)

    def add_consulter_toolbar(self):
        self.search_bar.show()
        self.search_text.show()
        self.order_text.show()
        self.order_selection.show()
        self.diapo_btn.show()
        self.modifier_btn.show()

    def remove_consulter_toolbar(self):
        self.search_bar.hide()
        self.search_text.hide()
        self.order_text.hide()
        self.order_selection.hide()
        self.diapo_btn.hide()
        self.modifier_btn.hide()

    @QtCore.pyqtSlot()
    def load_image(self):
        try:
            filename = next(self.filenames_iterator)
        except StopIteration:
            self.timer_loading.stop()
        else:
            name = os.path.basename(filename)
            it = QtWidgets.QListWidgetItem(name)
            it.fp = os.path.abspath(filename)
            pixmapOriginal = QtGui.QPixmap(filename)
            pixmap = pixmapOriginal.scaled(50, 50, QtCore.Qt.KeepAspectRatio)
            icon = QtGui.QIcon()
            icon.addPixmap(pixmap)
            it.setIcon(icon)
            self.pixmap_lw.addItem(it)

    def double_clicked(self, current):
        self.pixmap_lw.setCurrentItem(current)
        if self.currentpage_is_traiter:
            tmp = []
            for i in self.pixmap_lw.selectedItems():
                tmp.append(A_TRAITER_DIR + '\\' + i.text())
            self.pop = PopUpTraiter(tmp)
            self.pop.window_closed.connect(self.start_loading_a_traiter)
            self.pop.show()
        else:
            self.pop = Diaporama([current.fp])
            self.pop.showFullScreen()

    def search_request_handler(self):
        request = Request(self.search_bar.text(), self.current_filter)
        self.pixmap_lw.clear()
        for wi in request.process_to_widget():
            it = QListWidgetItem(self.pixmap_lw)
            it.fp = wi.idid
            it.setSizeHint(wi.sizeHint())
            self.pixmap_lw.addItem(it)
            self.pixmap_lw.setItemWidget(it, wi)

    def order_by_handler(self, i):
        self.current_filter = ORDER_BY_ARRAY[i]
        self.search_request_handler()

    def on_modify(self):
        if len(self.pixmap_lw.selectedItems()) == 0:
            return
        self.pop = PopUpModifier(self.pixmap_lw.itemWidget(self.pixmap_lw.selectedItems()[0]))
        self.pop.window_closed.connect(self.search_request_handler)
        self.pop.show()

    def on_diapo(self):
        tmp = []
        for i in range(self.pixmap_lw.count()):
            tmp.append(self.pixmap_lw.item(i).fp)
        self.pop = Diaporama(tmp)
        self.pop.showFullScreen()


if __name__ == "__main__":
    create_database()

    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
