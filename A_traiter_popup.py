import os

import shutil

from PyQt5 import QtWidgets, QtCore, QtGui, Qt
import sqlite3
from sqlite3 import Error

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QIntValidator

CONSULTER_DIR = ""


class PopUpTraiter(QtWidgets.QMainWindow):
    window_closed = pyqtSignal()

    def __init__(self, img_path: list, parent=None):
        super().__init__(parent)

        self.current = 0
        self.confirm_not_complete = False

        self.img_path = img_path
        self.datas = []
        for i in range(len(img_path)):
            self.datas.append(["", ""])
        print(img_path)

        self.filename = QtWidgets.QLabel()

        self.saisie_numero = QtWidgets.QLineEdit()
        self.saisie_titre = QtWidgets.QLineEdit()
        self.saisie_annee = QtWidgets.QLineEdit()
        self.saisie_annee.setValidator(QIntValidator())
        self.saisie_annee.setMaxLength(4)
        self.saisie_commentaire = QtWidgets.QTextEdit()
        self.saisie_commentaire.setMaximumHeight(50)

        self.prev_btn = QtWidgets.QPushButton("précédent")
        self.valider_btn = QtWidgets.QPushButton("valider")
        self.next_btn = QtWidgets.QPushButton("suivant")

        self.photo = QtWidgets.QLabel()
        self.photo.setText("")

        central_widget = QtWidgets.QWidget()
        self.setCentralWidget(central_widget)

        grid_layout = QtWidgets.QGridLayout(central_widget)

        grid_layout.addWidget(self.filename, 0, 0, 1, 3)
        grid_layout.addWidget(self.photo, 1, 0, 1, 3)
        grid_layout.addWidget(QtWidgets.QLabel("Numéro : "))
        grid_layout.addWidget(self.saisie_numero, 2, 1, 1, 2)
        grid_layout.addWidget(QtWidgets.QLabel("Titre : "), 3, 0, 1, 1)
        grid_layout.addWidget(self.saisie_titre, 3, 1, 1, 2)
        grid_layout.addWidget(QtWidgets.QLabel("Année : "), 4, 0, 1, 1)
        grid_layout.addWidget(self.saisie_annee, 4, 1, 1, 2)
        grid_layout.addWidget(QtWidgets.QLabel("Commentaire : "), 5, 0, 1, 1)
        grid_layout.addWidget(self.saisie_commentaire, 5, 1, 2, 2)
        grid_layout.addWidget(self.prev_btn, 7, 0, 1, 1)
        grid_layout.addWidget(self.valider_btn, 7, 1, 1, 1)
        grid_layout.addWidget(self.next_btn, 7, 2, 1, 1)

        # self.resize(640, 480)

        self.prev_btn.clicked.connect(self.on_prev)
        self.valider_btn.clicked.connect(self.on_valider)
        self.next_btn.clicked.connect(self.on_next)
        self.up_date()

    def on_prev(self):
        self.datas[self.current][0] = self.saisie_numero.text()
        self.datas[self.current][1] = self.saisie_commentaire.toPlainText()
        self.current = (self.current - 1) % len(self.img_path)
        self.up_date()

    def on_next(self):
        self.datas[self.current][0] = self.saisie_numero.text()
        self.datas[self.current][1] = self.saisie_commentaire.toPlainText()
        self.current = (self.current + 1) % len(self.img_path)
        self.up_date()

    def up_date(self):
        self.filename.setText(os.path.basename(self.img_path[self.current]))
        self.saisie_numero.setText(self.datas[self.current][0])
        self.saisie_commentaire.setText(self.datas[self.current][1])
        self.pixmapOriginal = QtGui.QPixmap(self.img_path[self.current])
        self.photo.setScaledContents(False)
        self.pixmap = self.pixmapOriginal.scaled(500, 500, QtCore.Qt.KeepAspectRatio)
        self.photo.setPixmap(self.pixmap)

    def on_valider(self):
        self.datas[self.current][0] = self.saisie_numero.text()
        self.datas[self.current][1] = self.saisie_commentaire.toPlainText()
        try:
            conn = sqlite3.connect("./GalleryDatabase.db")
            c = conn.cursor()
            for i in range(len(self.img_path)):
                query = '''insert into gallery (numero,imgpath,titre,annee,commentaire) values (?,?,?,?,?);'''
                tmp, extension = os.path.splitext(self.img_path[i])
                new_img_path = CONSULTER_DIR + '\\' + self.datas[i][0] + extension
                params = (self.datas[i][0], new_img_path, self.saisie_titre.text(), self.saisie_annee.text(),
                          self.datas[i][1])
                shutil.move(self.img_path[i], new_img_path)
                c.execute(query, params)
            conn.commit()
            conn.close()
            self.close()
        except Error as e:
            print(e)

    def closeEvent(self, event):
        self.window_closed.emit()
        event.accept()

